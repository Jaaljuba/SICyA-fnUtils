﻿using System;
using System.Collections.Generic;
//using System.Collections.Specialized;
using System.Configuration;
using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.DirectoryServices;
//using System.Web;
using System.Data;
using System.Data.SqlClient;

using Newtonsoft.Json;

namespace SICyA.fnUtils
{
    public class fnSICyA
    {
        public struct dataFiles
        {
            public string type;
            public byte[] bytes;
        }

        public SqlConnection sqlConnection;
        public SqlCommand sqlCommand;


        /// <summary>
        /// Function to get the app settings key
        /// </summary>
        /// <param name="pKey"></param>
        /// <returns></returns>
        public string getAppSettings(string pKey)
        {
            string strValue = string.Empty;

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                strValue = appSettings[pKey] ?? string.Empty;
            }
            catch (ConfigurationErrorsException ex)
            {
                saveErrorLog(ex);
            }

            return strValue;
        }


        /// <summary>
        /// Function to get connection string based in AppSettings key
        /// </summary>
        /// <param name="pKey"></param>
        /// <returns></returns>
        public string getConnString(string pKey)
        {
            string strConn = string.Empty;

            try
            {
                strConn = ConfigurationManager.ConnectionStrings[pKey].ConnectionString;
            }
            catch (Exception ex)
            {
                saveErrorLog(ex);
            }

            return strConn;
        }


        /// <summary>
        /// Function to save error log and send email (optional)
        /// </summary>
        /// <param name="pEx"></param>
        /// <param name="pSendMail"></param>
        public void saveErrorLog(Exception pEx, bool pSendMail = false)
        {
            StreamWriter fileLog;
            string strError, fileNameLog;

            fileNameLog = getAppSettings("FilePathErrors");
            if (fileNameLog != "")
            {
                try
                {
                    fileLog = new StreamWriter(fileNameLog, true);
                    strError = "Fecha y Hora: " + DateTime.Now.ToString() + "\r\n" +
                               "Usuario: " + "" +
                               "Mensaje: " + pEx.Message.ToString() + "\r\n" +
                               "Codigo: " + pEx.Source.ToString() + "\r\n" +
                               "Localizacion: " + pEx.StackTrace.ToString() + "\r\n\r\n";
                    fileLog.Write(strError);
                    fileLog.Close();
                }
                catch
                {
                    saveErrorLog("Error al grabar el registro de error!");
                }
            }

            if (pSendMail == true)
            {
                string nameFrom = getAppSettings("sendeMail_From");
                string mailFrom = getAppSettings("sendeMail_MailFrom");
                string mailTo = getAppSettings("sendeMail_MailTo");
                string subject = "Error en Aplicacion [eViajes]";
                string userMail = getAppSettings("sendeMail_User");
                string password = getAppSettings("sendeMail_Password");
                short port = Int16.Parse(getAppSettings("sendeMail_Port"));
                string host = getAppSettings("sendeMail_Host");
                bool activeSSL = (getAppSettings("sendeMail_SSL") == "T" ? true : false);

                //senderMail();
            }
        }

        public void saveErrorLog(string pMensaje, bool pSendMail = false)
        {
            StreamWriter fileLog;
            string strError, fileNameLog;

            fileNameLog = getAppSettings("FilePathErrors");
            if (fileNameLog != "")
            {
                try
                {
                    fileLog = new StreamWriter(fileNameLog, true);
                    strError = "Fecha y Hora: " + DateTime.Now.ToString() + "\r\n" +
                               "Usuario: " + "" +
                               "Mensaje: " + pMensaje + "\r\n\r\n";
                    fileLog.Write(strError);
                    fileLog.Close();
                }
                catch (Exception ex)
                {
                    saveErrorLog(ex);
                }
            }

            if (pSendMail == true)
            {
                string nameFrom = getAppSettings("sendeMail_From");
                string mailFrom = getAppSettings("sendeMail_MailFrom");
                string mailTo = getAppSettings("sendeMail_MailTo");
                string subject = "Error en AplicaciÃƒÂ³n [eViajes]";
                string userMail = getAppSettings("sendeMail_User");
                string password = getAppSettings("sendeMail_Password");
                short port = Int16.Parse(getAppSettings("sendeMail_Port"));
                string host = getAppSettings("sendeMail_Host");
                bool activeSSL = (getAppSettings("sendeMail_SSL") == "T" ? true : false);

                //senderMail();
            }
        }
        /// <summary>
        /// Graba en el servidor el archivo pasado por parametro
        /// </summary>
        /// <param name="pTypeName"></param>
        /// <param name="pPathFile"></param>
        /// <param name="pFile"></param>
        /// <returns></returns>
        public string saveImageServer(string pTypeName, string pPathFile, dataFiles pFile)
        {
            string fileName = pTypeName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            switch (pFile.type)
            {
                case "application/pdf":
                    fileName = fileName + "pdf";
                    break;
                case "application/jpeg":
                    fileName = fileName + "jpg";
                    break;
                case "application/png":
                    fileName = fileName + "png";
                    break;
            }

            try
            {
                if (pFile.bytes.Length > 0)
                {
                    if (File.Exists(pPathFile + fileName))
                    {
                        File.Delete(pPathFile + fileName);
                    }

                    using (FileStream fs = File.Create(pPathFile + fileName))
                    {
                        fs.Write(pFile.bytes, 0, pFile.bytes.Length);
                    }
                }
                else
                {
                    fileName = string.Empty;
                }
            }
            catch (Exception ex)
            {
                fileName = string.Empty;
                saveErrorLog(ex);
            }

            return fileName;
        }


        /// <summary>
        /// Function to execute query and return data in Json format
        /// </summary>
        /// <param name="pConnStr"></param>
        /// <param name="pCommType"></param>
        /// <param name="pQuery"></param>
        /// <param name="pParameters"></param>
        /// <returns>string</returns>
        /// <remarks>
        /// Return string with data in Json format
        /// </remarks>
        /// <history>
        /// [Jaaljuba] 22/08/2017 - Created
        /// </history>
        public string jsonFromSQL(string pConnStr, string pCommType, string pQuery, List<SqlParameter> pParameters = null)
        {
            SqlDataAdapter daData = new SqlDataAdapter();
            DataTable dtData = new DataTable();
            string strData = string.Empty;

            sqlConnection = new SqlConnection();
            sqlCommand = new SqlCommand();

            sqlConnection.ConnectionString = getConnString(pConnStr);

            try
            {
                sqlConnection.Open();

                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = pQuery;
                if (pCommType == "sp")
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                }
                else
                {
                    sqlCommand.CommandType = CommandType.Text;
                }

                sqlCommand.Parameters.Clear();
                foreach (SqlParameter parameter in pParameters)
                {
                    sqlCommand.Parameters.Add(parameter);
                }

                daData.SelectCommand = sqlCommand;
                daData.Fill(dtData);

                try
                {
                    strData = JsonConvert.SerializeObject(dtData);
                }
                catch (Exception ex)
                {
                    saveErrorLog(ex);
                }
            }
            catch (Exception ex)
            {
                saveErrorLog(ex);
            }

            return strData;
        }


        //    public string jsonFromSQL(string pCommType, string pQuery, params object[] pParameters)
        //    {
        //        SqlDataAdapter daData = new SqlDataAdapter();
        //        DataTable dtData = new DataTable();
        //        string strData = string.Empty;

        //        sqlConnection = new SqlConnection();
        //        sqlCommand = new SqlCommand();

        //        sqlConnection.ConnectionString = getConnString("strNameConn");

        //        try
        //        {
        //            sqlConnection.Open();

        //            sqlCommand.Connection = sqlConnection;
        //            sqlCommand.CommandText = pQuery;
        //            if (pCommType == "sp")
        //            {
        //                sqlCommand.CommandType = CommandType.StoredProcedure;
        //            }
        //            else
        //            {
        //                sqlCommand.CommandType = CommandType.Text;
        //            }

        //            sqlCommand.Parameters.Clear();
        //            if (pParameters.Length > 0)
        //            {
        //                sqlCommand.Parameters.AddRange(pParameters);
        //            }

        //            daData.SelectCommand = sqlCommand;
        //            daData.Fill(dtData);

        //            try
        //            {
        //                strData = JsonConvert.SerializeObject(dtData);
        //            }
        //            catch (Exception ex)
        //            {
        //                saveErrorLog(ex);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            saveErrorLog(ex);
        //        }

        //        return strData;
        //    }
    }
}
